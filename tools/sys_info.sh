report=sysinfo-`hostname`-`date | sed 's/ \|:/_/g'`
pwd=$PWD
targetdir=/tmp
basedir=$targetdir/$report
general=$basedir/general
logs=$basedir/logs
hardware=$basedir/hardware
network=$basedir/network
software=$basedir/software
roce=$basedir/roce
virtualization=$basedir/virtualization
script_errors=$basedir/errors.log

mkdir $basedir
sh `dirname $0`/debugfs/internal/mount_debugfs.sh 

#general
mkdir $general
hostname > $general/hostname
date > $general/date
whoami > $general/whoami
uptime > $general/uptime
echo "obtained general info"
#logs
mkdir $logs
dmesg > $logs/dmesg
ls /var/crash > $logs/crash
echo "obtained logs"

#hardware
mkdir $hardware
cat /proc/cpuinfo > $hardware/cpuinfo
cat /proc/meminfo > $hardware/meminfo
cat /proc/iomem > $hardware/iomem
cat /proc/pagetypeinfo > $hardware/pagetypeinfo
cat /proc/zoneinfo > $hardware/zoneinfo
cat /proc/buddyinfo > $hardware/buddyinfo
cat /proc/interrupts > $hardware/interupts
for irq in `ls /proc/irq`; do echo -n "irq $irq affinity "; cat /proc/irq/$irq/smp_affinity 2> /dev/null; done > $hardware/affinity
df -h > $hardware/df
lspci -xxxx -vvv > $hardware/lspci 2>> $script_errors
lspci -tv > $hardware/lspci-tv 2>> $script_errors
lscpu > $hardware/lscpu
dmidecode > $hardware/dmidecode
echo "obtained hardware info"

#software
mkdir $software
uname -a > $software/uname
cat /proc/cmdline > $software/cmdline
cp /lib/modules/`uname -a | cut -d" " -f3`/build/.config $software/config
lsb_release -a > $software/lsb_release 2>> $script_errors
lsmod > $software/lsmod
rpm -qa > $software/rpm
modinfo bnx2x > $software/modinfo-bnx2x 2>> $script_errors
modinfo qed > $software/modinfo-qed 2>> $script_errors
modinfo qede > $software/modinfo-qede 2>> $script_errors
service --status-all &> $software/services
echo "obtained software info"

#RoCE
mkdir $roce
modinfo qedr &>> $roce/roceinfo
if type ofed_info &>/dev/null; then
        ofed_info &>> $roce/roceinfo
else
        echo "INBOX OFED" >> $roce/roceinfo
fi
if type ibv_devinfo &>/dev/null; then
        ibv_devinfo -v  &>> $roce/roceinfo
else
        echo "ibv_devinfo missing" &>> $roce/roceinfo
fi
if type ib_write_bw &> /dev/null; then
        echo "ib_write_bw" &>> $roce/roceinfo
        ib_write_bw --version &>> $roce/roceinfo
else
        echo "ib_write_bw is missing" &>> $roce/roceinfo
fi
for QEDR in `ls /sys/kernel/debug/qedr 2> /dev/null`; do
	echo >> $roce/roceinfo
	echo $QEDR >> $roce/roceinfo
	cat /sys/kernel/debug/qedr/$QEDR/stats &>> $roce/roceinfo
done 
cp /etc/security/limits.conf $roce/limits.conf
cp -r /etc/security/limits.d $roce
cp /proc/sys/vm/max_map_count $roce
echo "obtained roce info"

#network
mkdir $network 
ip -d -d -o link show > $network/ip-link-show
ip address show > $network/ip-address-show
ifconfig -a > $network/ifconfig-a
route > $network/route
arp > $network/arp
netstat -s > $network/netstat-s
tc -s qdisc > $network/qdisc
for nic in `for i in \`ls /sys/bus/pci/devices/\`; do ls /sys/bus/pci/devices/$i/net 2> /dev/null; done`; do echo -e "\n$nic"; ethtool -i $nic; done > $network/ethtool-i
for nic in `for i in \`ls /sys/bus/pci/devices/\`; do ls /sys/bus/pci/devices/$i/net 2> /dev/null; done`; do echo -e "\n$nic"; ethtool -k $nic; done > $network/ethtool-k
for nic in `for i in \`ls /sys/bus/pci/devices/\`; do ls /sys/bus/pci/devices/$i/net 2> /dev/null; done`; do echo -e "\n$nic"; ethtool -S $nic; done > $network/ethtool-S
for nic in `for i in \`ls /sys/bus/pci/devices/\`; do ls /sys/bus/pci/devices/$i/net 2> /dev/null; done`; do echo -e "\n$nic"; ethtool -a $nic; done > $network/ethtool-a
for nic in `for i in \`ls /sys/bus/pci/devices/\`; do ls /sys/bus/pci/devices/$i/net 2> /dev/null; done`; do echo -e "\n$nic"; ethtool -g $nic; done > $network/ethtool-g
for nic in `for i in \`ls /sys/bus/pci/devices/\`; do ls /sys/bus/pci/devices/$i/net 2> /dev/null; done`; do echo -e "\n$nic"; ethtool $nic; done > $network/ethtool
cp -r /etc/sysconfig/network-scripts $network 2>> $script_errors
cp -r /etc/sysconfig/network/scripts $network 2>> $script_errors
`dirname $0`/debugfs/internal/nics.sh > $network/nics 2>> $script_errors
echo "obtained network info"

#virtualization
mkdir $virtualization
virsh list --all > $virtualization/virsh-list 2>> $script_errors
cp /sys/module/kvm/parameters/allow_unsafe_assigned_interrupts $virtualization/allow_unsafe_assigned_interrupts 2>> $script_errors
echo "obtained virtualization info"

echo report created under $basedir
cd $targetdir
tar czvf $report.tar.gz $report > /dev/null
cd $pwd

