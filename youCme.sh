#!/usr/bin/env sh
sudo apt-get install build-essential cmake vim-nox python3-dev
sudo apt-get install mono-complete golang nodejs default-jdk npm
cd ~/.vim/bundle/YouCompleteMe
python3 install.py --all

