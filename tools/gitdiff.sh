#!/usr/bin/env sh
git config diff.tool vimdiff
git config difftool.prompt false
git config alias.d difftool
git config core.editor vim
